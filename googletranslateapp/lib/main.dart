import 'package:flutter/material.dart';
import 'package:translator/translator.dart';

import 'models.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String translated = 'Translation';
  String textinput = '';
  // List<String> language = ['auto', 'en', 'id', 'ja'];

  Map<String, String> countries = {
    'Detect': 'auto',
    'English': 'en',
    'Indonesian': 'id',
    'Japanese': 'ja',
    'Korean': 'ko',
    'Spanish': 'es',
    'Arabic': 'ar',
    'Portuguese': 'pt',
    'Russian': 'ru',
    'Hindi': 'hi',
  };

  // String? selectedfromlang = 'auto';
  // String? selectedtolang = 'en';
  Country selectedfromlang = Country(name: 'English', code: 'en');
  Country selectedtolang = Country(name: 'Indonesian', code: 'id');

  @override
  Widget build(BuildContext context) {
    List<Country> countryList = [];
    countries.forEach((name, code) {
      countryList.add(Country(
          name: name,
          code: code)); //parse each object and add to the list of Countries
    });

    List<DropdownMenuItem<String>> countryListDropDown = [];
    for (Country country in countryList) {
      countryListDropDown.add(new DropdownMenuItem(
          value: country.name,
          child: Text(
            country.name,
            // style: TextStyle(fontSize: 20),
          )));
    }
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.translate),
        title: Text("Text Translator"),
      ),
      body: Card(
        margin: EdgeInsets.all(8),
        child: ListView(
          padding: EdgeInsets.all(15),
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                DropdownButton(
                    items: countryListDropDown,
                    value: selectedfromlang.name,
                    onChanged: (item) async {
                      setState(() {
                        selectedfromlang = countryList.firstWhere(
                            (loc) => loc.name == item,
                            orElse: () => countryList.first);
                      });
                      try {
                        final translation = await textinput.translate(
                          from: selectedfromlang.code,
                          to: selectedtolang.code,
                          // from: selectedfromlang.toString(),
                          // to: selectedtolang.toString(),
                        );
                        setState(() {
                          translated = translation.text;
                        });
                      } catch (e) {
                        print("error");
                        if (textinput != '') {
                          ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(content: Text(e.toString())));
                        }
                      }
                    }),
                // DropdownButton(
                //   value: selectedfromlang,
                //   items: language
                //       .map((item) =>
                //           DropdownMenuItem(value: item, child: Text(item)))
                //       .toList(),
                //   onChanged: (item) =>
                //       setState(() => selectedfromlang = item.toString()),
                // ),
                Text("to"),
                DropdownButton(
                  items: countryListDropDown,
                  value: selectedtolang.name,
                  onChanged: (item) async {
                    setState(() {
                      selectedtolang = countryList.firstWhere(
                          (loc) => loc.name == item,
                          orElse: () => countryList.first);
                    });
                    try {
                      final translation = await textinput.translate(
                        from: selectedfromlang.code,
                        to: selectedtolang.code,
                        // from: selectedfromlang.toString(),
                        // to: selectedtolang.toString(),
                      );
                      setState(() {
                        translated = translation.text;
                      });
                    } catch (e) {
                      print("error");
                      if (textinput != '') {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(e.toString())));
                      }
                    }
                  },
                ),
                // DropdownButton(
                //   value: selectedtolang,
                //   items: language
                //       .map((item) =>
                //           DropdownMenuItem(value: item, child: Text(item)))
                //       .toList(),
                //   onChanged: (item) =>
                //       setState(() => selectedtolang = item.toString()),
                // ),
              ],
            ),
            // Text("English (US)"),
            SizedBox(
              height: 8,
            ),
            TextField(
              onChanged: (text) async {
                try {
                  final translation = await text.translate(
                    from: selectedfromlang.code,
                    to: selectedtolang.code,
                    // from: selectedfromlang.toString(),
                    // to: selectedtolang.toString(),
                  );
                  setState(() {
                    textinput = text;
                    translated = translation.text;
                  });
                } catch (e) {
                  print("error");
                  ScaffoldMessenger.of(context)
                      .showSnackBar(SnackBar(content: Text(e.toString())));
                  setState(() {
                    translated = 'Translation';
                  });
                }
              },
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
              decoration: InputDecoration(
                hintText: "Enter text...",
              ),
            ),
            Divider(
              height: 32,
            ),
            Text(
              translated,
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}
