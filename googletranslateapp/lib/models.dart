import 'package:flutter/material.dart';

class Country {
  final String code;
  final String name;

  Country({required this.code, required this.name});
}
